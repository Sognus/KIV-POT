-------- PROJECT GENERATOR --------
PROJECT NAME :	bin2asc
PROJECT DIRECTORY :	D:\Workspace_POT\bin2asc
CPU SERIES :	2600
CPU TYPE :	Other
TOOLCHAIN NAME :	KPIT GNUH8 [ELF] Toolchain
TOOLCHAIN VERSION :	v11.02
GENERATION FILES :
    D:\Workspace_POT\bin2asc\start.asm
        Reset Program
    D:\Workspace_POT\bin2asc\inthandler.c
        Interrupt Handler
    D:\Workspace_POT\bin2asc\vects.c
        Vector Table
    D:\Workspace_POT\bin2asc\iodefine.h
        Definition of I/O Register
    D:\Workspace_POT\bin2asc\inthandler.h
        Interrupt Handler Declarations
    D:\Workspace_POT\bin2asc\hwinit.c
        Hardware Setup file
    D:\Workspace_POT\bin2asc\typedefine.h
        Aliases of Integer Type
    D:\Workspace_POT\bin2asc\intrinsic.h
        Intrinsic header file
    D:\Workspace_POT\bin2asc\bin2asc.c
        Main Program
START ADDRESS OF SECTION :
    0x000000800	.text,.rodata
    0x000FFEC00	.data,.bss
    0x00FFC000	.stack

SELECT TARGET :
    H8S/2600A Simulator
DATE & TIME : 6. 4. 2016 9:08:59
